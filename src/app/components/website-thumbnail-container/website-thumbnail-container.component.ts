import { ThumbnailHandlerService } from '../../services/component-handlers/thumbnail-handler.service';
import { ConfigService } from '../../services/config.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'website-thumbnail-container',
  templateUrl: './website-thumbnail-container.component.html',
  styleUrls: ['./website-thumbnail-container.component.css']
})
export class WebsiteThumbnailContainerComponent implements OnInit {

  public thumbnailDisplayInfo : any[];
  
  @Output()
  displayProject : EventEmitter<any> = new EventEmitter<any>();
  
  
  constructor(private thumbnailManager : ThumbnailHandlerService, public configService : ConfigService) { 
    this.thumbnailDisplayInfo = this.thumbnailManager.thumbnailConfiguration['configs'];
    
  }

  ngOnInit() {
  }
  
  loadProject(event) {
    this.displayProject.emit(event);
  }

}

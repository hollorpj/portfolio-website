import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteThumbnailContainerComponent } from './website-thumbnail-container.component';

describe('WebsiteThumbnailContainerComponent', () => {
  let component: WebsiteThumbnailContainerComponent;
  let fixture: ComponentFixture<WebsiteThumbnailContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsiteThumbnailContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteThumbnailContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

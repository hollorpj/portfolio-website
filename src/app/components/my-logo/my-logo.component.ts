import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'my-logo',
  templateUrl: './my-logo.component.html',
  styleUrls: ['./my-logo.component.css']
})
export class MyLogoComponent implements OnInit {

  @Output()
  scrollToProjects : EventEmitter<any> = new EventEmitter<any>();  
  
  constructor() { }

  ngOnInit() {
  }

  scroll() {
    this.scrollToProjects.emit();
  }
  
}

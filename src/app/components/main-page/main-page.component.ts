import { Component, OnInit, Renderer, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  @ViewChild('projectElement')
  public projects;
  
  public sectionsDisplayed : string[];
  
  public logoShown : boolean = true;
  
  
  constructor() {
    this.sectionsDisplayed = ['projects'];
  }

  ngOnInit() {}
 

  scrollProjects(projectElem) {
    this.projects.nativeElement.scrollIntoView({ behavior : 'smooth' });
  }
  
  triggerLogoDisplay(toDisplay) {
    this.logoShown = toDisplay;
  }
  
   areDisplayingSection(sectionId) {
    return this.sectionsDisplayed.indexOf(sectionId) != -1;
  }
  
  
  
}

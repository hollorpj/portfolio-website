import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'project-dossier',
  templateUrl: './project-dossier.component.html',
  styleUrls: ['./project-dossier.component.css']
})
export class ProjectDossierComponent implements OnInit {

  @Input()
  header: string;
  @Input()
  content: string;
  
  constructor() { 
    this.content = "This page was made for my entertainment.\n I have fun";
    this.header = "Header";
  }

  ngOnInit() {
  }

}

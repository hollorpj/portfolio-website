import { Component, OnInit, Input, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input()
  public height : string;
  @Input()
  public width : string;
  @Input()
  public type : string;
  
  @Input()
  public headers : string[];
  @Input()
  public content : string[];
  
  constructor(private renderer : Renderer, private elRef : ElementRef) {
  
  }

  ngOnInit() {
  }
  
  saveMessage(name, email, message) {
    console.log(name.value);
    
  }

}

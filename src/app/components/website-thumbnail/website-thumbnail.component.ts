import { ThumbnailHandlerService } from '../../services/component-handlers/thumbnail-handler.service';
import { ConfigService } from '../../services/config.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'website-thumbnail',
  templateUrl: './website-thumbnail.component.html',
  styleUrls: ['./website-thumbnail.component.css']
})
export class WebsiteThumbnailComponent implements OnInit {

  @Input()
  public thumbnailImageSource : string;
  @Input()
  public caption : string;
  // index to pull slides from the config service
  @Input() 
  public index : number;
  @Input()
  public url : string;
  
  @Output()
  displayProject : EventEmitter<any> = new EventEmitter();
  
  public thumbnailHeight : string;
  public thumbnailWidth : string;
  
  public containerHeight : string;
  public containerWidth : string;
  
  // value of thumbnailImageSource - To revert to after slideshow ends 
  public initialImage : string;
  public slideshow;
  
  constructor(private configService : ConfigService, private thumbnailService : ThumbnailHandlerService) {
    this.thumbnailHeight = this.configService.thumbnailHeight;
    this.thumbnailWidth = this.configService.thumbnailWidth;
    this.containerHeight = this.configService.containerHeight;
    this.containerWidth = this.configService.containerWidth;
    
  }

  ngOnInit() {
  }

  startSlideshow() {
    this.initialImage = this.thumbnailImageSource;
    
    const slides = this.thumbnailService.thumbnailConfiguration['configs'][this.index]['slides'];
    let slidePosition = 0;
    
    this.slideshow = setInterval(() => {
      slidePosition++;
      if (slidePosition === slides.length) {
        slidePosition = 0;
      }
      this.thumbnailImageSource = slides[slidePosition];
      
    }, 750)
  }
  
  endSlideshow() {
    this.thumbnailImageSource = this.initialImage;
    clearInterval(this.slideshow);
  }
  
  navigateToSite() {
    window.open(this.thumbnailService.thumbnailConfiguration['configs'][this.index]['url'], '_blank');
  }
  
  popOpenDescription() {
    window.open(this.url, '_blank');
  }
  
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteThumbnailComponent } from './website-thumbnail.component';

describe('WebsiteThumbnailComponent', () => {
  let component: WebsiteThumbnailComponent;
  let fixture: ComponentFixture<WebsiteThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsiteThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

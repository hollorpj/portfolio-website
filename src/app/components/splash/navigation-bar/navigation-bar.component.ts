import { NavigationBarService } from "../../../services/component-handlers/navigation-bar.service";
import { Component, OnInit, EventEmitter, Output, Renderer, ElementRef } from '@angular/core';

@Component({
  selector: 'navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  @Output()
  public triggerLogoDisplay : EventEmitter<boolean> = new EventEmitter<boolean>();
  
  public modalHeight;
  public modalWidth;
  public defaultHeight;
  public defaultWidth;
  
  public modalShown : boolean = false;
  public modalType : string;
  private currentModal : string = '';
  
  public modalContent : any;
  
  constructor(private navigationBarService : NavigationBarService) { 
     this.defaultHeight = '80vh';
     this.defaultWidth =  '60vw';
    
  }

  ngOnInit() {
  }
  
  toggleModal(sectionId : string) {
    if (!this.modalShown) {
      this.displayModal(sectionId);
    } else {
      if (this.currentModal != sectionId) {
        this.displayModal(sectionId);  
      } else {
        this.destructModal();
      }
    }
    
   
  }
  
  private displayModal(sectionId) {
    if (sectionId === 'contact') {
      this.modalType = 'form';
      this.modalHeight = '48vh';
      this.modalWidth = '32vw';
    } else {
      this.modalType = 'overview';
      this.modalHeight = this.defaultHeight;
      this.modalWidth = this.defaultWidth;
    }
     
    this.modalShown = true;
    this.currentModal = sectionId;
      
    this.triggerLogoDisplay.emit(!this.modalShown);
    this.modalContent = this.navigationBarService.getNavigationBarContent(sectionId);
    
    document.body.style.overflow='hidden';
  }
  
  private destructModal() {
    this.modalShown = false;
    this.currentModal = '';
    this.triggerLogoDisplay.emit(!this.modalShown);
    
    document.body.style.overflow='auto';
  }

}

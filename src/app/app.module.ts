import { AppRouterModule } from './app-router.route';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WebsiteThumbnailComponent } from './components/website-thumbnail/website-thumbnail.component';
import { ConfigService } from './services/config.service';
import { WebsiteThumbnailContainerComponent } from './components/website-thumbnail-container/website-thumbnail-container.component';
import { ThumbnailHandlerService } from './services/component-handlers/thumbnail-handler.service';
import { MainPageComponent } from './components/main-page/main-page.component';
import { ProjectDossierComponent } from './components/project-dossier/project-dossier.component';
import { MyLogoComponent } from './components/my-logo/my-logo.component';
import { NavigationBarComponent } from './components/splash/navigation-bar/navigation-bar.component';
import { ModalComponent } from './components/shared/modal/modal.component';
import { NavigationBarService } from "./services/component-handlers/navigation-bar.service";

@NgModule({
  declarations: [
    AppComponent,
    WebsiteThumbnailComponent,
    WebsiteThumbnailContainerComponent,
    MainPageComponent,
    ProjectDossierComponent,
    MyLogoComponent,
    NavigationBarComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule
  ],
  providers: [ ConfigService, ThumbnailHandlerService, NavigationBarService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

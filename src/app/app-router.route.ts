import { MainPageComponent } from './components/main-page/main-page.component';
import { WebsiteThumbnailComponent } from './components/website-thumbnail/website-thumbnail.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
		RouterModule.forRoot([
		{
			path: '',
			component: MainPageComponent
		}		
		])
	],
	exports: [
		RouterModule
	]
})

export class AppRouterModule {

}

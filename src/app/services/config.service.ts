import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  thumbnailHeight = '150px';
  thumbnailWidth = '256px';
  
  containerHeight = '220px';
  containerWidth = '256px';
  
  constructor() { }

}

import { Injectable } from '@angular/core';

@Injectable()
export class NavigationBarService {

  private navigationBarContent = {
    'philosophy' : {
      'headers' : ['A Hammer is for Nails, A Screwdriver is for Screws', 'To Cut Down a Tree in Five Minutes, Spend 3 Sharpening the Axe', 'Build Today for Tomorrow', 'Programming Languages come and go - Problem Solving is forever', 'Marketing is Part of the Job'],
      'content' : [
        'When all you have in your toolbox is a hammer, everything looks like a nail. Keeping a broad range of skills is imperative.\n' +
        'Being a good Java developer will not help you build the front-end of a website.\nBeing a good React/Angular/\'New cutting edge Javascript library\' developer will not help you build the back-end of a website.\n' +
        
        'Being a rock-star developer, who can code the entire project, will not help you understand what is important to your customers.\nPerspective is everything.',
      'Doing your due diligence in understanding the problem your solving, knowing how it impacts customers\n' +
      'and who needs to be involved is more valuable than jumping straight into a problem and delivering a solution.',
      'In an agile world, the code of an application is constantly in flux. The code that written today will be extended and\nrefined many times over the course of it\'s lifecycle. When writing software, I strive to write as generically and concisely as possible.\n' +
      'It not only makes the code easier to read/extend, but it also allows for re-use in other areas of the application.',
     'The languages/technologies we use today may not be around in a few years time.\nKnowing how to use the resources (tools/technologies) around you effectively, on the other hand, will always hold value.',
      'Most of the time, we won\'t bother with something new until we see it\'s value.\n' +
      'Being able to show people the value of a new solution is just as important as building said solution.'  ]
    },
    
    'summary' : {
      'headers' : ['Professional Summary'],
      'content' : [
                   'I am a well-rounded full-stack developer whom has developed software for multiple financial applications for which data errors are not an option and performance/reliability are crucial.\n\n' +
'As a result of these experiences, I take a scientific approach to software development. I learn the business requirements, find out which applications will be impacted, gather information on said systems and then formulate a development plan.\n\n' +
        'The development plan begins with a conceptual definition of how the functionality will be implemented. From here, I’ll think of ways to break the functionality and turn these into test cases. Some people like to call this TDD.\n\n' +
'Getting into the true development portion, I employ an umbrella-like approach.\n' + 
'When an umbrella is opened, the shaft extends to its furthest position and then the leaves of the umbrella billow out.\n' +
'Likewise, I first develop a prototype that encompasses all the major functions of the feature and then incrementally expand until the feature is ready.\n' +
'This allows for quick feedback from a very early point in development as well as easier modification/expansion.\nI like to call this approach \'the sprint within a sprint\'\n\n' +
        'Perhaps most important of all in this process is the concept of evolution. My approach to software development is not like following a recipe in a cookbook – The recipe is constantly being refined.\n' +
'At all times, I look for ways to improve the process and make it more streamlined. This may come in the form of a procedural modification, mindset change or building a tool.\n' +
'One thing remains certain – My throughput always increases over time.' ]
    },
    
    'contact' : {
      'headers' : [ 'E-mail', 'Linkedin'],
      'content' : [ 'hollorpj@gmail.com', 'https://www.linkedin.com/in/patrick-holloran-04582796/']
    }
  };
  
  constructor() { }
  
  getNavigationBarContent(id) {
    return this.navigationBarContent[id];
  }

}

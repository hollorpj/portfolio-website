import { TestBed, inject } from '@angular/core/testing';

import { ThumbnailHandlerService } from './thumbnail-handler.service';

describe('ThumbnailHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThumbnailHandlerService]
    });
  });

  it('should be created', inject([ThumbnailHandlerService], (service: ThumbnailHandlerService) => {
    expect(service).toBeTruthy();
  }));
});

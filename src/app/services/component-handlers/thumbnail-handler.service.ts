import { Injectable } from '@angular/core';

@Injectable()
export class ThumbnailHandlerService {

  thumbnailConfiguration = {
    'configs' : [
        {
          'imageSource' : '../../assets/images/thumbnails/secret-journal.png',
          'caption' : 'My Secret Journal',
          'slides' : ['../../assets/images/thumbnails/slideshow/secret-journal/secret-journal.png', '../../assets/images/thumbnails/slideshow/secret-journal/secret-journal-2.png', '../../assets/images/thumbnails/slideshow/secret-journal/secret-journal-3.png', '../../assets/images/thumbnails/slideshow/secret-journal/secret-journal-4.png', '../../assets/images/thumbnails/slideshow/secret-journal/secret-journal-5.png', '../../assets/images/thumbnails/slideshow/secret-journal/secret-journal-6.png'],
          'url' : 'https://my-secret-journal.herokuapp.com/',
          'projectInfo' : [
             {
               'heading' : 'Description',
               'content' :'This page was made for my entertainment.\n I have fun' 
             }
            ]
        },
        {
          'imageSource' : '../../assets/images/thumbnails/infinite-images.png',
          'slides' : ['../../assets/images/thumbnails/slideshow/infinite-images/infinite-images.png', '../../assets/images/thumbnails/slideshow/infinite-images/infinite-images-2.png', '../../assets/images/thumbnails/slideshow/infinite-images/infinite-images-3.png'],
          'caption' : 'Infinite Images',
          'url' : 'https://infinite-images-frontend.herokuapp.com'       
        },
        {
          'imageSource' : '../../assets/images/thumbnails/wedding-website.png',
          'slides' : ['../../assets/images/thumbnails/slideshow/wedding-website/wedding-website.png', '../../assets/images/thumbnails/slideshow/wedding-website/wedding-website-2.png', '../../assets/images/thumbnails/slideshow/wedding-website/wedding-website-3.png', '../../assets/images/thumbnails/slideshow/wedding-website/wedding-website-4.png'],
          'caption' : 'Wedding Website',
          'url' : 'http://patlovescheng.com/'
        }
      ]
  };
  
  constructor() { }

}
